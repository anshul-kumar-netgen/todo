<?php

namespace App\Http\Controllers;

use App\Http\Requests\TodoRequest;
use App\Models\Todo;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('todos.index');
    }

    /**
     * 
     *  Get Todos By Ajax
     * 
     */
    public function getTodosAjax(Request $request){
        if($request->ajax()){
            $query = Todo::query();
            return DataTables::of($query)
                            ->addColumn('action', function($row){
                                return '
                                <a href="" class="btn btn-sm btn-primary">Edit</a>
                                <a class="delete-data btn btn-sm btn-danger" title="Delete" href="javascript:void(0)" data-toggle="modal" data-target="#modelDelete" data-id="'.$row->id.'" data-b-toggle="tooltip" data-placement="bottom" data-original-title="Delete" >
                                    <i class="fa fa-trash"></i>
                                </a>
                                ';
                            })
                            ->editColumn('is_completed', function($row){
                                if($row->is_completed == true){
                                    return '<span class="badge badge-primary">Completed</span>';
                                }
                                else{
                                    return '<span class="badge badge-danger">Not Completed</span>';
                                }
                            })
                            ->rawColumns(['is_completed', 'action'])
                            ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('todos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(TodoRequest $request)
    {
        $res = (new Todo())->storeTodo($request);

        if($res['success'] == true){
            return redirect()->route('todos.index')->with('success', $res['message']);
        }
        else{
            return redirect()->back()->with('error', $res['message']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
