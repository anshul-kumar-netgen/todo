<?php

namespace App\Models;

use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    use HasFactory;

    /**
     * Store New Todo Item
     *
     * @param TodoRequest $request
     * @return array
     */
    public function storeTodo($request){
        try{
            $todo = new Todo;
            $todo->title = $request->title;
            $todo->is_completed = $request->is_completed;
            $res = $todo->save();

            if($res){
                return [
                    'success' => true,
                    'message' => 'Todo Item Saved'
                ];
            }
            else{
                return [
                    'success' => false,
                    'message' => 'Something Went Wrong, Try Again.'
                ];
            }
        }
        catch(Exception $e){
            return [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }
    }

}
