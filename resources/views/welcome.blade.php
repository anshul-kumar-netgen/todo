@extends('layouts.app')

@section('content')
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Inventore quaerat quasi obcaecati nostrum sint assumenda quam est quae quod distinctio, numquam at iusto unde repellat facilis deserunt veritatis expedita maxime!
    Magni quis, nihil tempora aliquam sed eveniet cupiditate soluta iusto consequuntur laboriosam quo, aperiam molestiae blanditiis delectus explicabo deleniti ut tenetur rem sequi officia alias molestias porro voluptates. Enim, numquam.
    Dolorem provident, officiis amet dignissimos esse soluta dolor voluptatibus temporibus deleniti ullam cupiditate iste aliquam laborum laudantium ducimus id accusantium sapiente dolorum eius magni. Harum consequatur non et quod enim?
    Eaque quos blanditiis molestias rem doloremque, totam reiciendis hic dolor laboriosam voluptas quo perspiciatis incidunt, maiores recusandae! Sit maiores mollitia reiciendis commodi tempora fuga, autem cumque veniam, eligendi vitae dolorem!
    Iste minima voluptas error illo dolor sint iusto voluptates ea aspernatur. Autem nihil, quia necessitatibus voluptas reiciendis enim, deleniti quibusdam, natus repellendus tempora aliquid cumque veniam ad iste perferendis? In?
    Vero quasi alias, officiis neque porro assumenda maxime minus quaerat praesentium ipsa velit accusantium corporis! Itaque voluptatem aperiam architecto, ipsum ipsam eius libero minima sed aliquam est laudantium! Sunt, quia.
    Quia pariatur laudantium doloribus aut. Distinctio totam assumenda suscipit voluptates ipsum asperiores dolores? Incidunt nam cumque dicta voluptatibus illum nostrum vel doloremque libero, accusantium vero facilis amet iste quis a.
    Natus similique magni quo corrupti magnam, omnis tenetur adipisci aspernatur enim modi laboriosam, doloremque perspiciatis voluptate laborum! Sed voluptas, ipsum possimus animi incidunt quia eius recusandae debitis odit assumenda soluta!
    Nulla laboriosam iure facilis libero odit tempore, cum asperiores? Ipsum, et, quo quis nihil rem cumque nisi ex ad modi, ab veniam vel suscipit. Ipsam accusantium quo eligendi inventore provident!
    Laborum aliquam, accusantium dolorem ex, hic non rerum in reiciendis delectus consequatur quam ducimus debitis saepe dicta cupiditate magnam sequi porro vitae error doloremque. Voluptatem, culpa porro! Ipsam, quibusdam veniam!
@endsection

@push('scripts')
    <script>
    </script>
@endpush