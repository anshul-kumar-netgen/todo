@extends('layouts.app')

@section('content')
    <div class="card my-2">
        <div class="card-header">
            Create Todos
            <a href="{{ route('todos.index') }}" class="btn btn-sm btn-dark float-right">Back</a>
        </div>
        <div class="card-body">
            <!-- Create Todos -->
            <form action="{{ route('todos.store') }}" method="post">
                @csrf
                <div class="form-group">
                  <label for="title">Title</label>
                  <input type="text" class="form-control" name="title" id="title" placeholder="Enter Title...">
                  @error('title')
                      <small class="text-danger">
                          <strong>
                            {{ $message }}
                          </strong>
                      </small>
                  @enderror
                </div>
                <div class="form-check form-check-inline">
                    <label class="form-check-label mx-2">
                        <input type="radio" class="form-check-input" name="is_completed" id="isCompleted" value="1" checked>
                        Yes
                    </label>
                    <label class="form-check-label mx-2">
                        <input type="radio" class="form-check-input" name="is_completed" id="isCompleted" value="0">
                        No
                    </label>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-primary btn-sm">Save</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
    </script>
@endpush